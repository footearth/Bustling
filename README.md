# Bustling

Bustling 是一个前端栅格化 (grid) 布局工具。

以后有可能会发展成为前端框架 (like bootstrap && pure) 乃至 前端解决方案 (like fis)。 --这个要看精力了和是否有后顾之忧了

另外项目内会有关于 bower & seajs（spm, cmd） & fis 的一些尝试 Demo，时机合适会分出去

## 发布

* 国内 gitoschina
* 国外 github

## 关于命名

* 参见 [Bustling 命名缘由](命名缘由)

## ToDo

移除 之前为了兼容 box 属性 及 不支持 flexbox 浏览器做的百分比方案，原因有二

1. css 代码臃肿堆砌不灵活（我有代码洁癖，大家见谅）
2. 百分比也并非能完全等分，bootstrap & pure 都有这样的问题
3. 之前在 fix 过程中已经使用了 js 作为辅助，于是打算这次 fix 完全使用 js

## Chang-log

- 0.0.1 (2013.07 - 2013.08)

    1. 2013 年 7 - 8 月，学习了 less && flexbox 之后发现 发现 bootstrap grid 没有使用 flexbox 属性，于是动手
    2. 2013 年 8 月 10 日，HTML 5 大会前夕 fix 完 flexbox 老版本 及 不支持 box 属性的兼容性问题

- 0.0.2 2014.01

    1. 2014 年 1 月，尝试了 pure 的 flex 之后各种原因，依然希望使用自己的 grid，于是开始重新修整

## 使用相关技术

## Some other

跟随先辈们的脚步，明知或迟或早会被拍死在沙滩上，还要继续栽树，让后人乘凉

## Hope

没有后顾之忧，全身心钻研技术，方向 全栈。。。
