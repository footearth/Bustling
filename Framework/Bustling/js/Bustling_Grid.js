$(function () {
    fixMixHeight();
    function fixMixHeight() {
        $(".wrap-col").each(function (index) {
            if ($(this).find(".row").length == 0) {
                var th = $(this).parent().innerHeight();
                var mt = 0;
                var mb = 0;
                if (Modernizr.flexbox) {
                    mt = parseInt($(this).css("margin-top").replace('px', ''));
                    mb = parseInt($(this).css("margin-bottom").replace('px', ''));
                } else {
                    mt = parseInt($(this).parent().css("padding-top").replace('px', ''));
                    mb = parseInt($(this).parent().css("padding-bottom").replace('px', ''));
                }
                var height = th - mt - mb + "px";
                $(this).css("height", height);
                $(this).css("line-height", height);
            }
        });
    }
});