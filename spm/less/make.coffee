#!/usr/bin/env coffee
require 'shelljs/make'

pkgname = 'less'
version = '1.5.1'

target.build = ->
  # exec 'spm-build'
  mkdir '-p', 'dist'
  cp "src/#{pkgname}-#{version}.js", "dist/#{pkgname}-debug.js"
  exec "uglifyjs dist/#{pkgname}-debug.js --comments --mangle -o dist/#{pkgname}.js"

target.clean = ->
  rm '-rf', 'dist'

target.publish = ->
  target.clean()
  target.build()
  exec 'spm publish -f'

target.all = ->
  target.clean()
