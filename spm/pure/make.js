// Generated by CoffeeScript 1.6.3
(function() {
  var pkgname, version;

  require('shelljs/make');

  pkgname = 'riot';

  version = '0.9.7';

  target.build = function() {
    mkdir('-p', 'dist');
    cp("src/" + pkgname + "-" + version + ".js", "dist/" + pkgname + "-debug.js");
    return exec("uglifyjs dist/" + pkgname + "-debug.js --comments --mangle -o dist/" + pkgname + ".js");
  };

  target.clean = function() {
    return rm('-rf', 'dist');
  };

  target.publish = function() {
    target.clean();
    target.build();
    return exec('spm publish -f');
  };

  target.all = function() {
    return target.clean();
  };

}).call(this);

/*
//@ sourceMappingURL=make.map
*/
