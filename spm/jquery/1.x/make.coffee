#!/usr/bin/env coffee
require 'shelljs/make'

pkgname = 'jquery'
version = '1.10.2'

target.build = ->
  exec 'spm-build'
  mv "dist/#{pkgname}-#{version}.js", "dist/#{pkgname}.js"
  mv "dist/#{pkgname}-#{version}-debug.js", "dist/#{pkgname}-debug.js"

target.clean = ->
  rm '-rf', 'dist'

target.publish = ->
  target.clean()
  target.build()
  exec 'spm publish -f'

target.all = ->
  target.clean()
