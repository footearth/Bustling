#!/usr/bin/env coffee
require 'shelljs/make'

pkg = [
  {
    pkgname: 'normalize'
    version: ''
  },
  {
    pkgname: 'jquery',
    version: '1.x'
  },
  {
    pkgname: 'jquery',
    version: '2.x'
  },
  {
    pkgname: 'lodash',
    version: ''
  },
  {
    pkgname: 'less'
    version: ''
  },
  {
    pkgname: 'zepto'
    version: ''
  },
  {
    pkgname: 'iscroll'
    version: ''
  },
  {
    pkgname: 'markdown'
    version: ''
  },
  {
    pkgname: 'riot'
    version: ''
  }
]

cdfoldexec = (inpath, outpath, callback) ->
  cd inpath
  callback()
  cd outpath

targetDo = (doSth) ->
  for s_pkg in pkg
    pn = s_pkg.pkgname
    vs = s_pkg.version
    inpath = "#{pn}/"
    outpath = '..'
    unless vs is ''
      inpath = "#{inpath}#{vs}/"
      outpath = "#{outpath}/.."

    cdfoldexec inpath, outpath, ->
      exec "shjs make.coffee #{doSth}"

target.build = ->
  targetDo 'build'

target.clean = ->
  targetDo 'clean'

target.publish = ->
  targetDo 'publish'
  target.clean()

target.all = ->
  target.clean()
