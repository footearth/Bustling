#!/usr/bin/env coffee
require 'shelljs/make'

pkgname_1 = 'html5shiv'
pkgname_2 = 'html5shiv-printshiv'
version = '3.7.1'

target.build = ->
  # exec 'spm-build'
  mkdir '-p', 'dist'

  cp "src/#{pkgname_1}-#{version}.js", "dist/#{pkgname_1}-debug.js"
  exec "uglifyjs dist/#{pkgname_1}-debug.js --comments --mangle -o dist/#{pkgname_1}.js"

  cp "src/#{pkgname_2}-#{version}.js", "dist/#{pkgname_2}-debug.js"
  exec "uglifyjs dist/#{pkgname_2}-debug.js --comments --mangle -o dist/#{pkgname_2}.js"

target.clean = ->
  rm '-rf', 'dist'

target.publish = ->
  target.clean()
  target.build()
  exec 'spm publish -f'

target.all = ->
  target.clean()
