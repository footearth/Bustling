#!/usr/bin/env coffee
require 'shelljs/make'

pkgname = 'normalize'
version = '2.1.3'

target.build = ->
  exec 'spm-build'

  mv "dist/#{pkgname}-#{version}.css", "dist/#{pkgname}.css"
  mv "dist/#{pkgname}-#{version}-debug.css", "dist/#{pkgname}-debug.css"

target.clean = ->
  rm '-rf', 'dist'

target.publish = ->
  target.clean()
  target.build()
  exec 'spm publish -f'

target.all = ->
  target.clean()
