#!/usr/bin/env coffee

module.exports = (grunt) ->

  # Constants
  dist_dir = 'dist/'
  temp_dir = 'temp/'

  # init
  grunt.initConfig

    clean:
      dist: [dist_dir]
      temp: [temp_dir]

    mkdir:
      dist:
        options:
          create: [dist_dir]
      temp:
        options:
          create: [temp_dir]

    bower:
      defalut:
        options:
          targetDir: temp_dir
          layout: 'byComponent'
          cleanup: true

    copy:
      defalut:
        expand: true
        cwd: temp_dir
        src: "**"
        dest: dist_dir
        flatten: false
        filter: 'isFile'

  # LoadTasks
  pkgs = [
    'grunt-contrib-clean',
    'grunt-mkdir',
    'grunt-contrib-copy',
    'grunt-bower-task'
  ]
  for pkg in pkgs
    grunt.loadNpmTasks pkg

  #  registerTask
  grunt.registerTask 'default', [
    'clean', 'mkdir', 'bower', 'copy', 'clean:temp'
  ]
