# NodeJs

* [Node 中文文档](http://nodeapi.ucdok.com/#/api/)
* [Node入门](http://www.nodebeginner.org/index-zh-cn.html)
* [从零开始nodejs](http://blog.fens.me/series-nodejs/)
* [使用 Express + MongoDB 搭建多人博客](https://github.com/nswbmw/N-blog)
* [使用 Express + Socket.IO 搭建多人聊天室](https://github.com/nswbmw/N-chat)
* [Node Tool Box](http://nodetoolbox.com/)

* shelljs/make

    将 GNU make 工具改为 shelljs/make ，并使用 coffee script 编码

    * 外部命令使用 exec ""

* [红豆园](http://yuan.rednode.cn/)