# YEOMAN

* [YEOMAN](http://yeoman.io/)
* [Package List](http://sindresorhus.com/bower-components/)

# Bower

* [官网](http://bower.io/)
* [中文](http://bower.jsbin.cn/)
* [bower解决js的依赖管理](http://blog.fens.me/nodejs-bower-intro/)
* [twitter 的包管理工具 - bower](http://chuo.me/2013/02/twitter-bower.html)
