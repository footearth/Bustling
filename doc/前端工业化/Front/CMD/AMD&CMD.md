# AMD

* [规范](https://github.com/amdjs/amdjs-api/wiki/AMD)

# CMD

* [规范](https://github.com/seajs/seajs/issues/242)

# 比较

* [SeaJS与RequireJS最大的区别](http://www.douban.com/note/283566440/)
    * [2](http://www.zhihu.com/question/20351507)
* [LABjs、RequireJS、SeaJS 哪个最好用？为什么？](http://www.zhihu.com/question/20342350)
* [与 RequireJS 的异同](https://github.com/seajs/seajs/issues/277)
