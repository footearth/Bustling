# [grunt-mkdir](https://github.com/rubenv/grunt-mkdir)

## package.json

    "grunt-mkdir": "~0.1.1",

## gruntfile.coffee

    mkdir:
        options:
            mode: '0700'
            create: []

## options.mode

接收一个4位数值，为创建目录权限设置

默认为 '0700'

## options.create

接受一个数组，为创建目录列表
