# [grunt-bower-task](https://github.com/yatskevich/grunt-bower-task)

## package.json

    "devDependencies": {
        "grunt": "~0.4.2",
        "grunt-bower-task": "~0.3.4",
    }

## gruntfile.coffee

    grunt.loadNpmTasks('grunt-bower-task');
