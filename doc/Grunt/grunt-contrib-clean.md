# [grunt-contrib-clean](https://github.com/gruntjs/grunt-contrib-clean)

## package.json

    "devDependencies": {
        "grunt": "~0.4.2",
        "grunt-contrib-clean": "~0.5.0",
    }


## gruntfile.coffee

    copy:
        options:
            force:
            no-write:
        files:


## Options.force

默认为 false

设置为 true 时，将强制执行

## Options.no-write

默认值 false

设置为 true 时，将不打印操作日志

## Example

    copy:
        taskname: []
