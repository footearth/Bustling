# Grunt

* [官网](http://gruntjs.com/)
    ||  [新手上路](http://gruntjs.com/getting-started)
    ||  [插件](http://gruntjs.com/plugins)
    ||  [API](http://gruntjs.com/api/grunt)
* == [中文官方](http://www.gruntjs.org/article/home.html) ==
* << [如何使用Grunt构建一个中型项目](https://github.com/twinstony/seajs-grunt-build) >>

## GruntFile

一个 Gruntfile 由下面几部分组成：

* "wrapper"函数(包装函数)
* 项目和任务配置
* 加载Grunt插件和任务
* 自定义任务

### gruntfile.coffee

    module.exports = (grunt) ->

        grunt.initConfig

        # LoadTasks
        pkgs = [
        ]
        for pkg in pkgs
          grunt.loadNpmTasks pkg

        grunt.registerTask 'default', []

### package.json

    "devDependencies": {
        "grunt": "~0.4.2",
    }

## API

### 插件

* 创建文件夹
    * [grunt-mkdir](https://github.com/rubenv/grunt-mkdir)
* 复制文件
    * [grunt-contrib-copy](https://github.com/gruntjs/grunt-contrib-copy)
* 清理文件 & 文件夹
    * [grunt-contrib-clean](https://github.com/gruntjs/grunt-contrib-clean)


* 替换字串 (静态资源路径)
    * [grunt-jade-usemin](https://github.com/pgilad/grunt-jade-usemin)

* Jade 2 Html
    * [grunt-contrib-jade](https://github.com/gruntjs/grunt-contrib-jade)
* Less 2 CSS
    * [grunt-contrib-less](https://github.com/gruntjs/grunt-contrib-less)
* CoffeScript 2 Js
    * [grunt-contrib-coffee](https://github.com/gruntjs/grunt-contrib-coffee)

* html语法验查
    * grunt-htmlhint
* js语法检查
    * grunt-contrib-jshint
* js合并
    * grunt-contrib-concat
* 采用UglifyJS压缩js
    * grunt-contrib-uglify
* Css压缩合并
    * grunt-contrib-cssmin

* CMD 依赖合并
    * grunt-cmd-concat
* CMD 提取依赖并设置模块ID
    * grunt-cmd-transport

* bower
    * [grunt-bower-task](https://github.com/yatskevich/grunt-bower-task)
* spm-grunt
* spm-chaos-build
