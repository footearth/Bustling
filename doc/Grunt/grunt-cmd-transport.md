# [grunt-cmd-transport](https://github.com/spmjs/grunt-cmd-transport)

递归目标js文件寻找所有以相对路径方式引用所依赖的模块，包括模块中嵌套引用的模块

接着对每一个模块进行 id 的生成和 dependence 的提取，最后把每一个模块生成到临时文件夹中（默认是当前目录的.build目录）

#### Options

* options.paths

    type: Array

    模块所在路径

* options.idleading

    type: String

    模块 ID

* options.alias

    type: Object

    模块别名

* options.debug

    type: Boolean

    Default: true

    是否创建 debug 文件

* options.handlebars

    type: Object

    指定编译处理器

* options.uglify

    type: Object

    压缩器优化选项

* options.parsers

    type: String Map

    指定扩展名文件解析器