# [grunt-contrib-copy](https://github.com/gruntjs/grunt-contrib-copy)

## package.json

    "grunt-contrib-copy": "~0.5.0",

## gruntfile.coffee

    copy:
        options:
            process: function (content, srcpath) {
                return content.replace(/[sad ]/g,"_");
            }
            noProcess: ""
            encoding: ""
            mode: false/true/0700
        files: [
            {},
            {}
        ]

## options.process

允许在复制的过程中做一些自定义操作

输入两个参数

content 为原文件内容

srcpath 为原文件路径

## options.noProcess

## options.encoding

默认值为 grunt.file.defaultEncoding

可以设置为其他字符集

## options.mode

默认为 false

设置为 true 时，复制后与原文件权限保持一致

设置为 4 位数值时，文件权限为设定值

## target

* expand 是否扩展
    * expand: true/false
* cwd 源文件夹
    * cwd: 'src/'
* src 源文件
    * src: '**'
* dest 目标文件夹
    * dest: 'dest/'
* flatten 是否保持源文件夹结构
    * false: 保持文件夹结构不变
    * true: 将所有文件放至目标目录
* filter 过滤器
    * filter: 'isFile'

## Example

    copy: {
      main: {
        files: [
          // includes files within path
          {expand: true, src: ['path/*'], dest: 'dest/', filter: 'isFile'},

          // includes files within path and its sub-directories
          {expand: true, src: ['path/**'], dest: 'dest/'},

          // makes all src relative to cwd
          {expand: true, cwd: 'path/', src: ['**'], dest: 'dest/'},

          // flattens results to a single level
          {expand: true, flatten: true, src: ['path/**'], dest: 'dest/', filter: 'isFile'}
        ]
      }
    }

## patch

    var options = this.options({
        Custdest: false
    });

    if ('function' === typeof options.desthdl) {
        dest = options.Custdest(src);
    }

    if (false === dest) {
        if (detectDestType(filePair.dest) === 'directory') {
            dest = (isExpandedPair) ? filePair.dest : unixifyPath(path.join(filePair.dest, src));
        } else
            dest = filePair.dest;
    } else {
        dest = unixifyPath(path.join(dest, src));
    }
