# [grunt-harp](https://github.com/shovon/grunt-harp)

## package.json

    "grunt-harp": "~0.4.1",

## gruntfile.coffee

    harp: {
        server: {
          server: true,
          source: 'src'
        },
        dist: {
          source: 'src',
          dest: 'build'
        }
    },

## options.server

是否启动一个服务器，默认 false

## options.source

指定源码路径

## options.dest

指定发布路径
