# [grunt-replace](https://github.com/outaTiME/grunt-replace)

## package.json

    "grunt-replace": "0.5.2"

## gruntfile.coffee

    replace:
        options:
            patterns: [
                {
                    match: 'foo',
                    replacement: 'bar', // replaces "@@foo" to "bar"
                    expression: false   // simple variable lookup
                }
                {
                    match: '/<%= grunt.template.date(847602000000, 'yyyy') %>/g',
                    replacement: '2014',    // replaces "1996" to "2014"
                    expression: true        // must be forced for templated regexp
                }
                {
                    match: /foo/g,
                    replacement: function () {
                        return 'bar';   // replaces "foo" to "bar"
                    }
                }
                {
                    json: {
                        "key": "value" // replaces "@@key" to "value"
                    }
                }
                {
                    json: {
                        "key": "value", // replaces "@@key" to "value"
                        "inner": {      // replaces "@@inner" with string representation of "inner" object
                            "key": "value"  // replaces "@@inner.key" to "value"
                        }
                    }
                }
                {
                    json: grunt.file.readJSON('config.json')
                }
            ]
            variables:
                'key': 'value' // replaces "@@key" to "value"
