# [grunt-contrib-jade](https://github.com/gruntjs/grunt-contrib-jade)

## package.json

    "grunt-contrib-jade": "~0.9.1",

## gruntfile.coffee

    copy:
        options:
            pretty: false
            data: (dest, src) ->
                return
                  from: src
                  to: dest
                // Return an object of data to pass to templates
                return require './locals.json'
            filters
                some: (block) ->
                    blabla
                another: (block) ->
                    blabla
                require('./filters.js')
            compileDebug: false
            client: false
            namespace: JST
            amd: false
            processName: (filename) ->
                blabla
        files: [
            {},
            {}
        ]
