# 关于 Winter

2013 HTML 5 大会那天接触了两位演讲者

一位是 被称为 计算机之子(他自己不喜欢别人如此称呼他，这里只是记录，请勿跨省) 的 Winter （程劭非）

另外一位 是 adobe 的 马鉴

与上述二位交流了一下关于 flexbox 的想法，但时间很短，并未对项目本身产生影响

以下摘录 一些资料

* [与众不同的微软签约历程](http://gongxue.cn/xiaoyuanwenhua/Print.asp?ArticleID=33516)
* [为什么程劭非被称为「计算机之子」？](http://www.zhihu.com/question/20359504)
* [计算机之子 winter （程劭非）老师为何一贯的挖坑不填？](http://www.zhihu.com/question/20435965)
* [Dart、CoffeeScript、TypeScript 和 JavaScript 哪种最适合专门学习？](http://www.zhihu.com/question/20833518)

Winter 那天讲了一下 touch 事件相关，标记，有时间研究一下
