# Vertical Centering With CSS 使用 CSS 实现垂直居中

![icon](imgs/icon.jpg)

There are a few different ways to vertically centre objects with CSS, but it can be difficult to choose the right one.
I’ll show you all the best ways I’ve seen and also how to create a nice little centered website.
Vertical centering with CSS isn’t an easy thing to do.

使用 CSS 实现垂直居中的效果有多种方案可供选择。
本文会带你寻找最合适的方式。
选择并不是一件容易的事情。

There are many different ways that may not work in some browsers.
Let’s review 5 different ways to vertically centering objects, as well as the pros and cons of each method.

其中有几种方式无法做到全浏览器兼容。
我们先来看下如下五种实现垂直居中的解决方案，并简述利弊。

(You can see my test page briefly explaining all of them.)

我会简单介绍一下每种实现 及其 Demo。

![demo](imgs/demo-thumb.jpg)

This method is very useful on small elements, such as to centre the text inside a button or single-line text field.

上图所示的方案，对于小元素 及 文字居中 效果不错。

### Method 1 方案一

This method sets some \<div\>s to display like a table,
so we can use the table’s vertical-align property (which works very differently for other elements).

方案一是将一些 div 元素的 display 属性 设置为 table。
我们可以使用 table 中 的 vertical-align 属性实现垂直居中。

```
<div id="wrapper">
	<div id="cell">
		<div class="content">
			Content goes here
		</div>
	</div>
</div>
```

```
#wrapper {
    display: table;
}
#cell {
    display: table-cell;
    vertical-align: middle;
}
```

#### The Goods 优势

* The content can dynamically change height (doesn’t have to be defined in CSS)
* Content doesn’t get cut off when there isn’t enough room in the wrapper
* 高度可以不固定，高度由内容决定，所以内容可以显示完整

#### The Bads 劣势

* Doesn’t work in Internet Explorer (not even the IE 8 beta)
* Lots of nested tags (not really that bad, this is a subjective topic)
* 不支持 IE。
* 多层嵌套结构，可能略显冗余，这个看个人了。

### Method 2 方案二

This method will use an absolutely positioned div,
which has the top set to 50% and the top margin set to negative half the height of the content.
This means the object must have a fixed height, that is defined by CSS.
此方案依赖 绝对定位 实现。
将 top 设为 50％，margin-top 设置为负的 父容器高度的50%。
这意味着该对象必须有一个固定的高度。

Because it has a fixed height, you may want to set overflow:auto; to the content div,
so if there is too much content to fit in, a scrollbar will appear,
instead of the content continuing on outside the div!
由于定死了 父容器 的高度，所以当内容过多时，会显示一个滚动条。

```
<div id="content">
	Content Here
</div>
```

```
/* negative half of the height */
#content {
    position: absolute;
    top: 50%;
    height: 240px;
    margin-top: -120px;
}
```

#### The Goods 优势

* Works in all browsers
* Doesn’t require nested tags
* 全浏览器兼容
* 不需要多层嵌套的结构

#### The Bads 劣势

* When there isn’t enough space, the content disappears
(such as when the div is inside the body and the user shrinks the browser window, a scrollbar will not appear)
* 如果内容过多，溢出的内容会看不到。

### Method 3

In this method, we will insert a div above the content element.
This will be set to height:50%; and margin-bottom:-contentheight;.
The content will then clear the float and end up in the middle.
方案三是通过在内容容器上插入一个浮动层实现的。
我们将浮动层的 height 设为 50%，margin-botton 设为负的 内容的一半高度。

```
<div id="floater">
    <div id="content">
        Content here
    </div>
</div>
```

```
#floater {
    float: left;
    height: 50%;
    margin-bottom: -120px;
}
#content {
    clear: both;
    height: 240px;
    position: relative;
}
```

#### The Goods 优势

* Works in all browsers
* When there isn’t enough space (ie. the window shrinks) our content will not be cut off, and a scrollbar will appear.
* 全浏览器兼容
* 当没有足够的空间（即窗口缩小），内容将不会被切断，会出现滚动条。

#### The Bads 劣势

* Only one I can think of is that it requires an extra empty element (which isn’t that bad, another subjective topic)
* 内容高度需要固定
* 需要插入一个额外的层达到效果

### Method 4 方案四

This method uses a position:absolute; div with a fixed width and height.
The div is then told to stretch to top:0; bottom:0;.
It can’t because of the fixed height, so margin:auto; will make it sit in the middle.
This is similar to using the very common margin:0 auto; to horizontally centre block elements.
这个方案也是用了 position: absolute。
固定了内容的 高宽，同时设定 top bottom left right 为 0，然后设置 margin: auto。
margin: 0 auto;

```
<div id="content">
	Content here
</div>
```

```
#content {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
	margin: auto;
	height: 240px;
	width: 70%;
}
```

#### The Goods 优势

* Easy
* 简单方便

#### The Bads

* Doesn’t work in Internet Explorer (does work in IE8 Beta)
* Content is cut off without scrollbar if there isn’t enough room in the container
* 不支持 IE
* 高宽固定，内容超出容器会被截断

### Method 5 方案五

This method will only centre a single line of text.
Simply set the line-height to the height of the object, and the text sits in the middle
此方案只适用于文本，将容器的 line-height 设为容器的高度。

```
<div id="content">
	Content here
</div>
```

```
#content {
    height: 100px;
    line-height: 100px;
}
```

#### The Goods 优势

* Works in all browsers
* Doesn’t get cut off when the isn’t enough space
* 全浏览器兼容
* 高度固定，超出容器会被截断

#### The Bads 劣势

* Only works on text (no block elements)
* When there is more than a single line (like when it wraps), it breaks badly
* 只适用与文本这种非块状元素
* 只适用与单行场景，多行场景则不适用

## Which Method? 究竟应该选择哪种方案？

My favourite method is number 3 — using a floater and clearing the content.
It doesn’t have any major downsides.
Because the content will clear:both;, you can also put other elements above it, and when the windows collapses,
the centered content will not cover them up. See the demo.


```
<div id="top">
	<h1>Title</h1>
</div>
<div id="floater"></div>
<div id="content">
	Content Here
</div>
```

```
#floater {
    float: left;
    height: 50%;
    margin-bottom: -120px;}

#top {
    float: right;
    width: 100%;
    text-align: center;
}

#content {
    clear: both;
    height: 240px;
    position: relative;
}
```

Now you know how it works, let’s start creating a simple but interesting website!
The final product will look something like this:

![Step4](imgs/step4-thumb.jpg)

### Step 1

It’s always good to start with semantic markup. This is how our page will be structured:

* \#floater (to push the content into the middle)
* \#centred (the centre box)
    * \#side
        * \#logo
        * \#nav (unordered list <ul\>)
    * \#content
* \#bottom (for copyright, etc.)

Here is the xhtml code I will be using:

```
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>A Centred Company</title>
	<link rel="stylesheet" href="styles.css" type="text/css" media="all" />
</head>

<body>
	<div id="floater"></div>
	<div id="centered">

		<div id="side">
			<div id="logo"><strong><span>A</span> Company</strong></div>
			<ul id="nav">
				<li><a href="#">Home</a></li>
				<li><a href="#">Products</a></li>
				<li><a href="#">Blog</a></li>
				<li><a href="#">Contact</a></li>
				<li><a href="#">About</a></li>
			</ul>
		</div>

		<div id="content">

			<h1>Page Title</h1>

			<p>
			Holisticly re-engineer value-added outsourcing after process-centric collaboration and idea-sharing.
			Energistically simplify impactful niche markets via enabled imperatives.
			Holisticly predominate premium innovation after compelling scenarios.
			Seamlessly recaptiualize high standards in human capital with leading-edge manufactured products.
			Distinctively syndicate standards compliant schemas before robust vortals.
			Uniquely recaptiualize leveraged web-readiness vis-a-vis out-of-the-box information.
			</p>

			<h2>Heading 2</h2>

			<p>
			Efficiently embrace customized web-readiness rather than customer directed processes.
			Assertively grow cross-platform imperatives vis-a-vis proactive technologies.
			Conveniently empower multidisciplinary meta-services without enterprise-wide interfaces.
			Conveniently streamline competitive strategic theme areas with focused e-markets.
			Phosfluorescently syndicate world-class communities vis-a-vis value-added markets.
			Appropriately reinvent holistic services before robust e-services.
			</p>

		</div>

	</div>

	<div id="bottom">
		<p>
			Copyright notice goes here
		</p>
	</div>
</body>
</html>
```

### Step 2

Now we will start with some basic CSS to lay out the page.
You should put this in styles.css, which is linked to at the top of our html.

```
html, body {
	margin:0; padding:0;
	height:100%;
}

body {
	background:url('page_bg.jpg') 50% 50% no-repeat #FC3;
	font-family:Georgia, Times, serifs;
}

#floater {
	position:relative; float:left;
	height:50%;	margin-bottom:-200px;
	width:1px;
}

#centered {
	position:relative; clear:left;
	height:400px; width:80%; max-width:800px; min-width:400px;
	margin:0 auto;
	background:#fff;
	border:4px solid #666;
}

#bottom {
	position:absolute;
	bottom:0; right:0;
}

#nav {
	position:absolute; left:0; top:0; bottom:0; right:70%;
	padding:20px; margin:10px;
}

#content {
	position:absolute; left:30%; right:0; top:0; bottom:0;
	overflow:auto; height:340px;
	padding:20px; margin:10px;
}
```

Before we can make our content vertically centred, the body and html must be stretched to 100% height.
Because the height is inside the padding and margin,
we have to make them 0 so a scrollbar doesn’t appear just to show you a little margin.

The floater’s margin-bottom is half of the content’s height (400px), which is -200px.

You should now have something that looks like this:

![Step2](imgs/step2-thumb.jpg)

### Step 3

The last thing to do is add some more styles to make it look a bit nicer. Lets start with the menu.

```
#nav ul {
	list-style:none;
	padding:0; margin:20px 0 0 0; text-indent:0;
}

#nav li {
	padding:0; margin:3px;
}

#nav li a {
	display:block; background-color:#e8e8e8;
	padding:7px; margin:0;
	text-decoration:none; color:#000;
	border-bottom:1px solid #bbb;
	text-align:right;
}

#nav li a::after {
	content:'»'; color:#aaa; font-weight:bold;
	display:inline; float:right;
	margin:0 2px 0 5px;
}

#nav li a:hover, #nav li a:focus {
	background:#f8f8f8;
	border-bottom-color:#777;
}

#nav li a:hover::after {
	margin:0 0 0 7px; color:#f93;
}

#nav li a:active {
	padding:8px 7px 6px 7px;
}
```

The first thing to do when turning a list into a menu kind of thing is to remove the dot points with list-style:none
 and all the margin and padding.
If you want it to have a margin or padding, make sure you specify exactly what,
don’t leave it to the web browsers defaults because they can vary.

The next thing to notice is that the links are set to display as a block element.
This makes them fill the entire line and gives you more control over them.
If you want to make your menu go horizontally (doesn’t work in this design),
then you can make them float as well.

The other interesting thing to notice about the menu is the :before and :after CSS pseudo-element
 let you insert content before and after elements.
This is a good way to include little icons or characters such as the arrow at the end of each link.
This doesn’t work in Internet Explorer before version 8 though.

![Step3](imgs/step3-thumb.jpg)

### Step 4

The last thing to do is add some more CSS to make the page look a bit nicer.

```
#centered {
	-webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px;
}

h1, h2, h3, h4, h5, h6 {
	font-family:Helvetica, Arial, sans-serif;
	font-weight:normal; color:#666;
}

h1 {
	color:#f93; border-bottom:1px solid #ddd;
	letter-spacing:-0.05em; font-weight:bold;
	margin-top:0; padding-top:0;
}


#bottom {
	padding:10px;
	font-size:0.7em;
	color:#f03;
}


#logo {
	font-size:2em; text-align:center;
	color:#999;
}

#logo strong {
	font-weight:normal;
}

#logo span {
	display:block;
	font-size:4em; line-height:0.7em;
	color:#666;
}

p, h2, h3 {
	line-height:1.6em;
}

a {
	color:#f03;
}
```

A thing to notice is the rounded corners on #centered.
In CSS3, there should be a border-radius property to set the radius of rounded corners.
This is not implemented by any major browsers yet,
unless you use the -moz or -webkit prefixes (for Mozilla Firefox and Safari/Webkit)

![Step4](imgs/step4-thumb.jpg)

### Compatibility Notes

As you might have guessed, Internet Explorer is the only main browser which gives you trouble:

* The #floater must have a width defined, or it doesn’t do anything in any version of IE
* IE 6 has too much space around our menu, which breaks it
* IE 8 has extra space above the

### More Ideas

There are many interesting things you can do with centered websites.
I used this idea in my redesign of the SWFObject Generator 2.0 (to generate code to use SWFObject 2.0).
Here is also another idea.

### Sources

I did not discover all this myself.
Here are some of the articles I have read which describe some of these techniques:
(If you are interested, I recommend you read them)

* Understanding vertical-align, or "How (Not) To Vertically Center Content"
* Vertical centering using CSS
* Vertical Centering in CSS
