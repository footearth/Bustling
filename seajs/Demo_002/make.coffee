#!/usr/bin/env coffee
require 'shelljs/make'

target.build = ->
  exec 'spm install seajs'

target.clean = ->
  rm '-rf', './sea-modules/'

target.deploy = ->
  target.clean()
  target.build()

target.all = ->
  target.clean()
