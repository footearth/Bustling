#!/usr/bin/env coffee
require 'shelljs/make'

HomeDir = '.'
seaModD = "#{HomeDir}/sea-modules"
distD = 'static/dist'

familyN = 'examples'
modN = 'ng-todo'
version = '1.0.0'

modD = "#{seaModD}/#{familyN}/#{modN}"
deployD = "#{modD}/#{version}"

target.init = ->
  echo 'install seajs ...'
  exec 'spm install seajs'

  echo 'install jquery ...'
  exec 'spm install jquery'

  echo 'install gallery/store ...'
  exec 'spm install gallery/store'

  echo 'install angular/angularjs ...'
  exec 'spm install angular/angularjs'

target.build = ->
  cd 'static'
  exec 'spm build'
  cd '..'

target.clean = ->
  rm '-rf', "#{seaModD}"
  rm '-rf', "#{distD}"

target.deploy = ->
  echo 'cleaning ...'
  target.clean()

  echo 'initing ...'
  target.init()

  echo 'building ...'
  target.build()

  echo 'deploy start ...'
  rm '-rf', "#{modD}"
  mkdir '-p', "#{deployD}"
  cp "#{distD}/*", "#{deployD}"
  echo ''
  echo "deploy to #{deployD} done."
  echo ''

target.all = ->
  target.clean()
